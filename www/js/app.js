// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
'use strict';
angular.module('starter', [
  'ionic',
  'starter.controllers',
  'auth0',
  'angular-storage',
  'angular-jwt'
])

.run(function($ionicPlatform, $location, auth, $rootScope, store, jwtHelper, $state, $timeout) {

  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    auth.hookEvents();
  });

  $rootScope.$on('$locationChangeStart', function() {
    if (!auth.isAuthenticated) {
      var token = store.get('token');
      store.set('access_token', null);
      if (token) {
        if (!jwtHelper.isTokenExpired(token)) {
          auth.authenticate(store.get('profile'), token);
        } else {
          auth.refreshIdToken(refreshToken).then(function(idToken) {
            store.set('token', idToken);
            auth.authenticate(store.get('profile'), idToken);
            return idToken;
          });
        }
      }
    }
  });

  $rootScope.$on('$locationChangeSuccess', function() {
    if (auth.isAuthenticated) {
      $timeout(function() {
        $rootScope.$broadcast('reload:' + $state.current.name);
      }, 0);
    }
  });
})

.config(function($provide, $stateProvider, $urlRouterProvider, authProvider, jwtInterceptorProvider, $httpProvider) {
  $stateProvider

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'AppCtrl',
    data: {
      requiresLogin: true
    }
  })

  .state('login', {
    url: "/login",
    templateUrl: "templates/login.html",
    controller: 'LoginCtrl'
  })

  .state('app.routes', {
    url: "/routes",
    views: {
      'menuContent': {
        controller: 'RoutesCtrl',
        templateUrl: "templates/routes.html"
      }
    }
  })

  .state('app.discover', {
    url: "/discover",
    views: {
      'menuContent': {
        controller: 'DiscoverCtrl',
        templateUrl: "templates/discover.html"
      }
    }
  })

  .state('app.profile', {
    url: "/profile/:id",
    views: {
      'menuContent': {
        templateUrl: "templates/profile.html",
        controller: 'ProfileCtrl'
      }
    }
  })

  .state('app.friends', {
    url: "/friends",
    views: {
      'menuContent': {
        templateUrl: "templates/friends.html",
        controller: 'FriendsCtrl'
      }
    }
  })

  .state('app.messages', {
    url: "/messages",
    views: {
      'menuContent': {
        templateUrl: "templates/messages.html",
        controller: 'MessagesCtrl'
      }
    }
  })

  .state('app.compose', {
    url: "/messages/compose",
    views: {
      'menuContent': {
        templateUrl: "templates/compose.html",
        controller: 'MessagesComposeCtrl'
      }
    }
  })

  .state('app.chat', {
    url: "/messages/chat/:id",
    views: {
      'menuContent': {
        templateUrl: "templates/chat.html",
        controller: 'ChatCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/discover');

  authProvider.init({
    domain: 'socialbus.auth0.com',
    clientID: 'LxEo4JrdSx3fpVIbRh9yGNLfWCnajgI4',
    callbackURL: location.href,
    loginState: 'login'
  });

  jwtInterceptorProvider.tokenGetter = function(store, jwtHelper, auth) {
    var idToken = store.get('token');
    var refreshToken = store.get('refreshToken');
    // If no token return null
    if (!idToken || !refreshToken) {
      return null;
    }

    // Get access_token
    if (!store.get('access_token')) {
      $.post('https://socialbus.auth0.com/oauth/token', {
        'client_id': 'LxEo4JrdSx3fpVIbRh9yGNLfWCnajgI4',
        'client_secret': 'yFvkFGSW--7pKkXugmsvoHWVyB8qQZqQcIOYo497tL9ukHw7L7UbnyxRP-NziYC4',
        'grant_type': 'client_credentials'
      }, function(data) {
        store.set('access_token', data.access_token);
      });
    }

    // If token is expired, get a new one
    if (jwtHelper.isTokenExpired(idToken)) {
      return auth.refreshIdToken(refreshToken).then(function(idToken) {
        store.set('token', idToken);
        return idToken;
      });
    } else {
      return idToken;
    }
  };

  $httpProvider.interceptors.push('jwtInterceptor');
})

.service('Messages', function($http, store, $timeout, Users) {
  var messages = {};

  var getMessages = function() {
    $timeout(function() {
      var profile = store.get('profile');
      if (profile && profile.user_id) {
        var userId = profile.user_id;

        $http.get('https://api.mongolab.com/api/1/databases/socialbus/collections/messages?apiKey=sAsd-LJVmrcTa_1-b_nL5SaDkuScCfoe&q={"$or":[{"to":"' + userId + '"},{"from":"' + userId + '"}]}').then(function(response) {
          var _messages = {};
          _.each(response.data, function(message) {
            if (message.from == userId && message.from) {
              if (!_messages[message.to]) {
                _messages[message.to] = [];
              }
              message.linkId = message.to;
              message.profile = Users.get(message.to);
              if (!message.profile) {
                $.get('https://socialbus.auth0.com/api/users/' + message.to + '?access_token=' + store.get('access_token'), function(data) {
                  Users.save(data);
                  message.profile = data;
                  _messages[message.to].push(message);
                });
              } else {
                _messages[message.to].push(message);
              }
            } else if (message.to == userId && message.to) {
              if (!_messages[message.from]) {
                _messages[message.from] = [];
              }
              message.linkId = message.from;
              message.profile = Users.get(message.from);
              if (!message.profile) {
                $.get('https://socialbus.auth0.com/api/users/' + message.from + '?access_token=' + store.get('access_token'), function(data) {
                  Users.save(data);
                  message.profile = data
                  _messages[message.from].push(message);
                });
              } else {
                _messages[message.from].push(message);
              }
            }
          });
          messages = _messages;
        });
      }
      getMessages();
    }, 1500);
  };
  getMessages();

  return {
    get: function(userId) {
      if (userId) {
        return messages[userId];
      } else {
        return messages;
      }
    }
  };
})

.service('Users', function($http) {
  var users = {};
  return {
    save: function(data) {
      users[data.user_id] = data;
    },
    get: function(id) {
      var sentRequests = JSON.parse((localStorage.getItem('sentRequests') || '{}'));
      if (users[id] && users[id].isFriend && sentRequests[id]) {
        delete sentRequests[id];
        localStorage.setItem('sentRequests', JSON.stringify(sentRequests));
      }
      return users[id];
    }
  };
});
