'use strict';

angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $location, auth, store, Messages) {
  var isNotFirstTime = localStorage.getItem('isNotFirstTime');

  if (!isNotFirstTime) {
    $ionicModal.fromTemplateUrl('templates/no_routes.html', {
      scope: $scope
    }).then(function(modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });
  }

  $scope.closeModal = function() {
    $scope.modal.hide();
    $timeout(function() {
      $location.path('/app/routes');
    }, 0);
  };

  $scope.logout = function() {
    auth.signout();
    store.remove('profile');
    store.remove('token');
    location.reload();
  };
})

.controller('NewRouteCtrl', function($scope, $http, store) {
  var userId = store.get('profile').user_id;
  $scope.rutas = [];

  $http.get('/rutas/guadalajara.json').then(function(response) {
    $scope.rutas = response.data;
  });

  $scope.close = function() {
    $scope.city = 'Guadalajara';
    $scope.route = 'R-101 Vía 1';
    $scope.from = '5';
    $scope.to = '5';
    $scope.$parent.getRoutes(true);
    $scope.$parent.closeModal();
  };

  $scope.saveRoute = function(_data) {
    if (!_data.city) {
      alert('Seleccione una ciudad');
      return;
    }

    if (!_data.route) {
      alert('Seleccione una ruta');
      return;
    }

    if (!_data.from) {
      alert('Seleccione una hora de inicio');
      return;
    }

    if (!_data.to) {
      alert('Seleccione una hora final');
      return;
    }

    if (_data.to <= _data.from) {
      alert('La hora final debe ser mayor que la inicial');
      return;
    }

    if (_data.to-_data.from >= 2) {
      alert('La duración en la ruta no puede ser mayor a 2 horas');
      return;
    }

    var data = {
      city: _data.city,
      route: _data.route,
      from: _data.from,
      to: _data.to,
      userId: userId
    };

    $http.post('https://api.mongolab.com/api/1/databases/socialbus/collections/user_routes?apiKey=sAsd-LJVmrcTa_1-b_nL5SaDkuScCfoe', data).then(function() {
      $scope.$parent.getRoutes(true);
    });
    $scope.close();
  };
})

.controller('RoutesCtrl', function($scope, $ionicModal, $http, store, $ionicLoading) {
  var userId = store.get('profile').user_id,
    isNotFirstTime = localStorage.getItem('isNotFirstTime');
  $scope.items = [];

  $scope.getString = function(hour) {
    var string;
    if (hour.match(/\.5/)) {
      string = hour.replace(/\.5/, ':30');
    } else {
      string = hour + ':00';
    }

    if (parseInt(hour, 10) > 11) {
      string += 'pm';
    } else {
      string += 'am';
    }
    return string;
  };

  $scope.getRoutes = function(skipLoader) {
    $scope.loaded = false;

    if (!isNotFirstTime) {
      localStorage.setItem('isNotFirstTime', true);
      return;
    }

    if (!skipLoader) {
      $ionicLoading.show({
        template: 'Obteniendo rutas...'
      });
    }

    $http.get('https://api.mongolab.com/api/1/databases/socialbus/collections/user_routes?apiKey=sAsd-LJVmrcTa_1-b_nL5SaDkuScCfoe&q={"userId":"' + userId + '"}').then(function(response) {
      $scope.items = response.data || [];
      $scope.loaded = true;

      if (!skipLoader) {
        $ionicLoading.hide();
      }
    });
  };
  $scope.getRoutes();

  $scope.$on('reload:app.routes', function() {
    $scope.getRoutes(true);
  });

  $ionicModal.fromTemplateUrl('templates/new_route.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.hours = [];
  function buildHours() {
    for (var i = 5; i <= 23; i += 0.5) {
      var str;
      if ((i + '').match(/\.5/)) {
       str = (i + '').replace('.5', ':30');
      } else {
       str = i + ':00';
      }

      if (i > 11) {
        str += ' pm';
      } else {
        str += ' am';
      }

      $scope.hours.push({
       id: i,
       str: str
      });
    }
  }
  buildHours();

  $scope.showAddRoute = function() {
    $scope.modal.show();
  };

  $scope.closeModal = function() {
    $scope.modal.hide();
  };
})

.controller('FriendsCtrl', function($scope, store, $http, Users) {
  var userId = store.get('profile').user_id;
  $scope.users = {};
  $scope.loading = true;

  $scope.getFriendRequests = function() {
    $http.get('https://api.mongolab.com/api/1/databases/socialbus/collections/friend_requests?apiKey=sAsd-LJVmrcTa_1-b_nL5SaDkuScCfoe&q={"to":"'+ userId +'"}').then(function(response) {
      $scope.friendRequests = response.data;

      _.each($scope.friendRequests, function(item) {
        if (!$scope.users[item.userId]) {
          $scope.users[item.userId] = {};
          $.get('https://socialbus.auth0.com/api/users/'+item.userId+'?access_token=' + store.get('access_token'), function(data) {
            $scope.users[item.userId].data = data;
            item.profile = $scope.users[item.userId];
            Users.save(response.data);
            if (!$scope.$$phase) {
              $scope.$apply();
            }
          });
        } else {
          item.profile = $scope.users[item.userId];
        }
      });
      $scope.loading = false;
    });
  };
  $scope.getFriendRequests();

  $scope.getFriends = function() {
    $http.get('https://api.mongolab.com/api/1/databases/socialbus/collections/user_friends?apiKey=sAsd-LJVmrcTa_1-b_nL5SaDkuScCfoe&q={"userId":"'+ userId +'"}').then(function(response) {
      $scope.friends = response.data;

      _.each($scope.friends, function(item) {
        if (!$scope.users[item.friendId]) {
          $scope.users[item.friendId] = {};
          $.get('https://socialbus.auth0.com/api/users/'+item.friendId+'?access_token=' + store.get('access_token'), function(data) {
            data.isFriend = true;
            $scope.users[item.friendId].data = data;
            item.profile = $scope.users[item.friendId];
            Users.save(data);
            if (!$scope.$$phase) {
              $scope.$apply();
            }
          });
        } else {
          item.profile = $scope.users[item.friendId];
        }
      });
      $scope.loading = false;
    });
  };
  $scope.getFriends();

  $scope.removeRequest = function(request) {
    var id = request._id.$oid;
    $http.delete('https://api.mongolab.com/api/1/databases/socialbus/collections/friend_requests/' + id + '?apiKey=sAsd-LJVmrcTa_1-b_nL5SaDkuScCfoe').then(function() {
      $scope.getFriendRequests();
    });
  };

  $scope.acceptRequest = function(request) {
    $scope.removeRequest(request);
    var data = {
      userId: userId,
      friendId: request.profile.data.user_id
    };
    $http.get('https://api.mongolab.com/api/1/databases/socialbus/collections/user_friends?apiKey=sAsd-LJVmrcTa_1-b_nL5SaDkuScCfoe&q={"userId":"'+ userId +'","friendId":"'+ data.friendId + '"}').then(function(responseIsFriend) {
      var isFriend = !!responseIsFriend.data.length;
      if (!isFriend) {
        $http.post('https://api.mongolab.com/api/1/databases/socialbus/collections/user_friends?apiKey=sAsd-LJVmrcTa_1-b_nL5SaDkuScCfoe', { userId: data.friendId, friendId: data.userId });
        $http.post('https://api.mongolab.com/api/1/databases/socialbus/collections/user_friends?apiKey=sAsd-LJVmrcTa_1-b_nL5SaDkuScCfoe', data).then(function() {
          $scope.getFriends();
        });
      }
    });
  };
})

.controller('ProfileCtrl', function($scope, $stateParams, store, Users, $http) {
  var sentRequests = JSON.parse((localStorage.getItem('sentRequests') || '{}'));
  var userId = store.get('profile').user_id;
  $scope.profile = store.get('profile');
  $scope.userId = null;
  $scope.sentRequest = false;

  $scope.initProfile = function() {
    if (parseInt($stateParams.id, 10) === 0) {
      $scope.userId = $stateParams.id;
    } else {
      $scope.profile = Users.get($stateParams.id) || {};
      $scope.sentRequest = sentRequests[$scope.profile.user_id];
    }
  };

  $scope.initProfile();

  $scope.$on('reload:app.profile', function() {
    $scope.initProfile();
  });

  $scope.getGender = function() {
    if ($scope.profile.gender === 'male') {
      return 'Hombre';
    } else if ($scope.profile.gender === 'female') {
      return 'Mujer';
    } else {
      return ':)';
    }
  };

  $scope.sendFriendRequest = function() {
    if ($scope.sentRequest) return;
    var data = {
      userId: userId,
      to: $scope.profile.user_id
    };
    sentRequests[$scope.profile.user_id] = true;
    localStorage.setItem('sentRequests', JSON.stringify(sentRequests));
    $http.post('https://api.mongolab.com/api/1/databases/socialbus/collections/friend_requests?apiKey=sAsd-LJVmrcTa_1-b_nL5SaDkuScCfoe', data).then(function() {});
    $scope.sentRequest = true;
  };
})

.controller('MessagesComposeCtrl', function($scope, $http, store, auth, Users) {
  var userId = store.get('profile').user_id;
  $scope.users = {};
  $scope.loading = true;

  $scope.getFriends = function() {
    $http.get('https://api.mongolab.com/api/1/databases/socialbus/collections/user_friends?apiKey=sAsd-LJVmrcTa_1-b_nL5SaDkuScCfoe&q={"userId":"'+ userId +'"}').then(function(response) {
      $scope.friends = response.data;

      _.each($scope.friends, function(item) {
        if (!$scope.users[item.friendId]) {
          $scope.users[item.friendId] = {};
          $.get('https://socialbus.auth0.com/api/users/'+item.friendId+'?access_token=' + store.get('access_token'), function(data) {
            data.isFriend = true;
            $scope.users[item.friendId].data = data;
            item.profile = $scope.users[item.friendId];
            Users.save(data);
            if (!$scope.$$phase) {
              $scope.$apply();
            }
          });
        } else {
          item.profile = $scope.users[item.friendId];
        }
      });
      $scope.loading = false;
    });
  };

  $scope.getFriends();
  $scope.$on('reload:app.messages', function() {
    $scope.getFriends();
  });

})

.controller('MessagesCtrl', function($scope, $http, store, auth, Users, $timeout, Messages) {
  var userId = store.get('profile').user_id;
  $scope._ = _;
  $scope.messages = [];
  $scope.loading = true;

  $scope.getMessages = function() {
    var messages = Messages.get();
    if (messages != $scope.messages) {
      $scope.messages = messages;
    }
    $scope.loading = false;
  };


  $scope.getLoop = function() {
    $timeout(function() {
      $scope.getMessages();
      $scope.getLoop();
    }, 1500);
  };

  $scope.getMessages();
  $scope.getLoop();

  $scope.$on('reload:app.messages', function() {
    $scope.getMessages();
  });

})

.controller('ChatCtrl', function($scope, $http, store, auth, Users, $stateParams, $timeout, Messages) {
  $scope.me = store.get('profile');
  $scope.profile = Users.get($stateParams.id) || {};
  $scope.messages = [];

  $scope.getMessages = function() {
    var messages = Messages.get($stateParams.id);
    if (messages != $scope.messages) {
      $scope.messages = messages;
    }
    $timeout(function() {
      $scope.getMessages();
    }, 1000);
  };

  $scope.sendMessage = function() {
    var data = {
      from: $scope.me.user_id,
      to: $stateParams.id,
      date: new Date(),
      message: $scope.message,
      profile: $scope.me
    };

    $http.post('https://api.mongolab.com/api/1/databases/socialbus/collections/messages?apiKey=sAsd-LJVmrcTa_1-b_nL5SaDkuScCfoe', data);
    data.me = true;
    $scope.messages.push(data);
    $scope.message = "";
  };

  $scope.getMessages();
  $scope.$on('reload:app.messages', function() {
    $scope.getMessages();
  });

})

.controller('DiscoverCtrl', function($scope, $http, store, auth, Users) {
  var userId = store.get('profile').user_id;
  $scope.routes = [];
  $scope.users = {};
  $scope.loading = true;

  $scope.getString = function(hour) {
    var string;
    if (hour.match(/\.5/)) {
      string = hour.replace(/\.5/, ':30');
    } else {
      string = hour + ':00';
    }

    if (parseInt(hour, 10) > 11) {
      string += 'pm';
    } else {
      string += 'am';
    }
    return string;
  };
  window.auth = auth;

  $scope.getMyRoutes = function() {
    var foundRoutes = [];
    $http.get('https://api.mongolab.com/api/1/databases/socialbus/collections/user_routes?apiKey=sAsd-LJVmrcTa_1-b_nL5SaDkuScCfoe&q={"userId":"' + userId + '"}').then(function(response) {
      var routes = response.data;
      var uniqueUsers = {};

      if (!routes.length) {
        $scope.loading = false;
        return;
      }

      _.each(routes, function(route) {
        // Get users with the same route
        var query = '{"userId":{"$ne":"'+ userId +'"},"route":"'+route.route+'","from":{"$lt":"' + route.to + '"},"to": {"$gt":"'+ route.from +'"}}';
        $http.get('https://api.mongolab.com/api/1/databases/socialbus/collections/user_routes?apiKey=sAsd-LJVmrcTa_1-b_nL5SaDkuScCfoe&q=' + query).then(function(response) {
          var users = response.data;

          // Get user profiles
          _.each(users, function(item) {
            if (uniqueUsers[item.userId + ':' + item.route]) {
              return;
            } else {
              uniqueUsers[item.userId + ':' + item.route] = true;
            }

            if (!$scope.users[item.userId]) {
              $scope.users[item.userId] = {};
              $.get('https://socialbus.auth0.com/api/users/'+item.userId+'?access_token=' + store.get('access_token'), function(data) {
                $http.get('https://api.mongolab.com/api/1/databases/socialbus/collections/user_friends?apiKey=sAsd-LJVmrcTa_1-b_nL5SaDkuScCfoe&q={"userId":"'+ userId +'","friendId":"'+ item.userId + '"}').then(function(responseIsFriend) {
                  data.isFriend = !!responseIsFriend.data.length;
                  $scope.users[item.userId].data = data;
                  item.profile = $scope.users[item.userId];
                  Users.save(data);
                  if (!$scope.$$phase) {
                    $scope.$apply();
                  }
                });
              });
            } else {
              item.profile = $scope.users[item.userId];
            }
            foundRoutes.push(item);
          });

          $scope.routes = _.groupBy(foundRoutes, function(item) {
            return item.route;
          });
          $scope.loading = false;
        });
      });
    });
  };
  $scope.getMyRoutes();
})

.controller('LoginCtrl', function($scope, $http, auth, $state, store, $ionicModal, $location, $timeout) {
  $ionicModal.fromTemplateUrl('templates/about.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modalAbout = modal;
  });

  $scope.about = function() {
    $scope.modalAbout.show();
  };

  $scope.closeModal = function() {
    $scope.modalAbout.hide();
  };

  // $scope.doLogin = function () {
  //   auth.signin({
  //     popup: true,
  //     connection: 'facebook'
  //     // scope: 'openid name email'
  //   }, onLoginSuccess, onLoginFailed);
  // };

  // function onLoginFailed() {
  //   alert('Error al iniciar sesión');
  // }

  // function onLoginSuccess(profile, token, state, refreshToken) {
  //   store.set('profile', profile);
  //   store.set('token', token);
  //   store.set('refreshToken', refreshToken);
  //   $state.go('app.discover');
  // }

  auth.signin({
    connection: ['twitter', 'facebook'],
    authParams: {
      // This asks for the refresh token
      // So that the user never has to log in again
      scope: 'openid offline_access',
      // This is the device name
      device: 'Mobile device'
    },
    // Make the widget non closeable
    standalone: true
  }, function(profile, token, accessToken, state, refreshToken) {
    // Login was successful
    // We need to save the information from the login
    store.set('profile', profile);
    store.set('token', token);
    store.set('refreshToken', refreshToken);
    $state.go('app.discover');
  }, function(error) {
    // Oops something went wrong during login:
    console.log("There was an error logging in", error);
  });

  $scope.translateLogin = function() {
    $('.a0-panel').hide();
    $timeout(function() {
      $('.a0-last-time').html('La vez anterior iniciaste sesión como...');
      $('.a0-all').html('Acceder con otra cuenta').bind('click', function() {
        $scope.translateLogin();
      });

      $('.logo').appendTo($('.a0-icon-container'));
      $('.a0-header h1').html('Iniciar sesión');
      $('.a0-panel').fadeIn('slow');
    }, 500);
  };

  $('.a0-panel').hide();
  $timeout(function() {
    $scope.translateLogin();
  }, 500);
});
